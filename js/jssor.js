﻿//Jssor slider
var mq = window.matchMedia( "(min-width: 960px)" );
if (mq.matches) {
	//For Jssor_1
	jssor_1_slider_init = function() {

		var jssor_1_options = {
			$AutoPlay : true,
			$AutoPlaySteps : 1,
			$SlideDuration : 160,
			$SlideWidth : 198,
			$SlideSpacing : 3,
			$Cols : 5,
			$ArrowNavigatorOptions : {
				$Class : $JssorArrowNavigator$,
				$Steps : 1
			},
			$BulletNavigatorOptions : {
				$Class : $JssorBulletNavigator$,
				$SpacingX : 2,
				$SpacingY : 2
			}
		};

		var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

		ScaleSlider();
		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		//responsive code end
	};

	//For Jssor_2
	jssor_2_slider_init = function() {

		var jssor_2_options = {
			$AutoPlay : true,
			$AutoPlaySteps : 1,
			$SlideDuration : 160,
			$SlideWidth : 198,
			$SlideSpacing : 3,
			$Cols : 5,
			$ArrowNavigatorOptions : {
				$Class : $JssorArrowNavigator$,
				$Steps : 1
			},
			$BulletNavigatorOptions : {
				$Class : $JssorBulletNavigator$,
				$SpacingX : 2,
				$SpacingY : 2
			}
		};

		var jssor_2_slider = new $JssorSlider$("jssor_2", jssor_2_options);

		ScaleSlider();
		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		//responsive code end
	};

	//For Jssor_3
	jssor_3_slider_init = function() {

		var jssor_3_options = {
			$AutoPlay : true,
			$AutoPlaySteps : 1,
			$SlideDuration : 160,
			$SlideWidth : 295,
			$SlideSpacing : 3,
			$Cols : 3,
			$ArrowNavigatorOptions : {
				$Class : $JssorArrowNavigator$,
				$Steps : 1
			},
			$BulletNavigatorOptions : {
				$Class : $JssorBulletNavigator$,
				$SpacingX : 2,
				$SpacingY : 2
			}
		};

		var jssor_3_slider = new $JssorSlider$("jssor_3", jssor_3_options);

		ScaleSlider();
		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		//responsive code end
	};

	//For Jssor_4
	jssor_4_slider_init = function() {

		var jssor_4_options = {
			$AutoPlay : true,
			$AutoPlaySteps : 1,
			$SlideDuration : 160,
			$SlideWidth : 198,
			$SlideSpacing : 3,
			$Cols : 5,
			$ArrowNavigatorOptions : {
				$Class : $JssorArrowNavigator$,
				$Steps : 1
			},
			$BulletNavigatorOptions : {
				$Class : $JssorBulletNavigator$,
				$SpacingX : 2,
				$SpacingY : 2
			}
		};

		var jssor_4_slider = new $JssorSlider$("jssor_4", jssor_4_options);

		ScaleSlider();
		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		//responsive code end
	};

	//For Jssor_6
	jssor_6_slider_init = function() {

		var jssor_6_options = {
			$AutoPlay : true,
			$AutoPlaySteps : 1,
			$SlideDuration : 160,
			$SlideWidth : 300,
			$SlideSpacing : 3,
			$Cols : 4,
			$ArrowNavigatorOptions : {
				$Class : $JssorArrowNavigator$,
				$Steps : 1
			},
			$BulletNavigatorOptions : {
				$Class : $JssorBulletNavigator$,
				$SpacingX : 2,
				$SpacingY : 2
			}
		};

		var jssor_6_slider = new $JssorSlider$("jssor_6", jssor_6_options);

		ScaleSlider();
		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		//responsive code end
	};
} else {
	//For Jssor_1
	jssor_1_slider_init = function() {

		var jssor_1_options = {
			$AutoPlay : true,
			$AutoPlaySteps : 1,
			$SlideDuration : 160,
			$SlideWidth : 139,
			$SlideSpacing : 3,
			$Cols : 5,
			$ArrowNavigatorOptions : {
				$Class : $JssorArrowNavigator$,
				$Steps : 1
			},
			$BulletNavigatorOptions : {
				$Class : $JssorBulletNavigator$,
				$SpacingX : 2,
				$SpacingY : 2
			}
		};

		var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

		ScaleSlider();
		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		//responsive code end
	};

	//For Jssor_2
	jssor_2_slider_init = function() {

		var jssor_2_options = {
			$AutoPlay : true,
			$AutoPlaySteps : 1,
			$SlideDuration : 160,
			$SlideWidth : 139,
			$SlideSpacing : 3,
			$Cols : 5,
			$ArrowNavigatorOptions : {
				$Class : $JssorArrowNavigator$,
				$Steps : 1
			},
			$BulletNavigatorOptions : {
				$Class : $JssorBulletNavigator$,
				$SpacingX : 2,
				$SpacingY : 2
			}
		};

		var jssor_2_slider = new $JssorSlider$("jssor_2", jssor_2_options);

		ScaleSlider();
		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		//responsive code end
	};

	//For Jssor_3
	jssor_3_slider_init = function() {

		var jssor_3_options = {
			$AutoPlay : true,
			$AutoPlaySteps : 1,
			$SlideDuration : 160,
			$SlideWidth : 139,
			$SlideSpacing : 3,
			$Cols : 3,
			$ArrowNavigatorOptions : {
				$Class : $JssorArrowNavigator$,
				$Steps : 1
			},
			$BulletNavigatorOptions : {
				$Class : $JssorBulletNavigator$,
				$SpacingX : 2,
				$SpacingY : 2
			}
		};

		var jssor_3_slider = new $JssorSlider$("jssor_3", jssor_3_options);

		ScaleSlider();
		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		//responsive code end
	};

	//For Jssor_4
	jssor_4_slider_init = function() {

		var jssor_4_options = {
			$AutoPlay : true,
			$AutoPlaySteps : 1,
			$SlideDuration : 160,
			$SlideWidth : 139,
			$SlideSpacing : 3,
			$Cols : 5,
			$ArrowNavigatorOptions : {
				$Class : $JssorArrowNavigator$,
				$Steps : 1
			},
			$BulletNavigatorOptions : {
				$Class : $JssorBulletNavigator$,
				$SpacingX : 2,
				$SpacingY : 2
			}
		};

		var jssor_4_slider = new $JssorSlider$("jssor_4", jssor_4_options);

		ScaleSlider();
		$Jssor$.$AddEvent(window, "load", ScaleSlider);
		$Jssor$.$AddEvent(window, "resize", ScaleSlider);
		$Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
		//responsive code end
	};
}
